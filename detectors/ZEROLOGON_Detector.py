
import re

from attacks_detector import Attacks_Detector

class ZEROLOGON_Detector(Attacks_Detector):

	ELK_CONFIG = [{ 'host': 'localhost', 'port': 9200 }]

	def __init__(self):
		super().__init__(ZEROLOGON_Detector.ELK_CONFIG, "Zerologon")

	###########################
	### DETECTION OF PSEXEC ###
	###########################


	DETECTION_ZEROLOGON = [
		{
			"Value": 10,
			"Optionnal": False,
			"Conditions": {
				"must": {
					"System.EventID#": 5805,
					"System.ProviderName": "NETLOGON"
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"System.Computer",
				"Data.RawData"
			],
			"Transform": {
				"Transform.Authenticator": {
					"Data.RawData": r"^(.*?),"
				}
			},
			"Details": { "Success": "Authentications failed by access denied (5805)" }
		},
		{
			"Value": 10,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4742,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.SubjectUserName": "ANONYMOUS LOGON",
					"range_time": [ -1000, 1000 ]
				}
			},
			"Fields": [
				"Data.PasswordLastSet",
				"Data.TargetLogonId",
				"Data.TargetDomainName",
				"Data.TargetUserName",
				"Data.TargetSid"
			],
			"Details": { "Success": "A computer account was changed (4742)" }
		},
		{
			"Value": 0,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 3,
					"System.ProviderName": "Microsoft-Windows-Sysmon",
					"System.Computer": "System.Computer",
					"Data.DestinationHostname": "System.Computer",
					"Data.Image": "C:\\\\Windows\\\\System32\\\\lsass.exe",
					"range_time": [ 0, 5000 ]
				},
				"must_not": {
					"Data.SourceHostname": "System.Computer"
				}
			},
			"Fields": [
				"Data.UtcTime",
				"Data.User",
				"Data.DestinationHostname",
				"Data.DestinationIp",
				"Data.DestinationPort#",
				"Data.SourceHostname",
				"Data.SourceIp",
				"Data.SourcePort#"
			],
			"Details": { "Success": "SYSMON - TCP to Lsass.exe (3)" }
		}
	]



	def _display_detection_list_(self, arg_detection):

		for key in arg_detection:
			if key == "Probability":
				print(" "+chr(0x21B3)+f"  Probability : {int(arg_detection[key]*100)} %")
			elif key == "Details":
				tmp = arg_detection[key].replace('\n', '\n    '+chr(0x21B3)+' ')
				print(f"  | {key} : {self.DETECTION_NAME} detection\n    "+chr(0x21B3)+f" {tmp}")
			elif "Time" in key or "time" in key:
				print(f"  | {key} : {self._date_from_timestamp_millis_(arg_detection[key])}")
			else:
				print(f"  | {key} : {arg_detection[key]}")


	def _test_attack_detection_(self, arg_index, arg_detection_list, PART_NAME="", VERBOSITY=1):

		if VERBOSITY > 0: print(f"[ ] Detection of Zerologon attack ({PART_NAME})...")

		if isinstance(arg_index, list):
			result = []
			for index in arg_index:
				result_tmp = self._test_attack_(index, arg_detection_list)
				if not isinstance(result_tmp, int):
					for detection in result_tmp: result.append(detection)
		else: result = self._test_attack_(arg_index, arg_detection_list)
		if isinstance(result, int): result = []

		if isinstance(result, int):
			if result != 0: print(f'[-] Error during the detection of Zerologon attack ({PART_NAME}) !')
		elif len(result) == 0:
			if VERBOSITY > 1: print(f"[+] Nothing detected ({PART_NAME}) !")
		elif VERBOSITY > 1:
			print(f"[+] Traces of Zerologon attack detected ({PART_NAME}) !")
			if VERBOSITY > 2:
				print('')
				for detection in result:
					self._display_detection_list_(detection)
					print('')

		return result

	def _traitment_datas_(self, arg_result, VERBOSITY=1):

		if VERBOSITY > 0: print("[ ] Traitment of received datas...")
		
		attacks = []
		regex_timestamp_decomp = re.compile("^@timestamp[0-9]*_([0-9]*)$")
		regex_key_number_decomp = re.compile("^(.*)_([0-9]*)$")
		result = sorted(arg_result, key=lambda r: int(r['@timestamp']))	# Sort by timestamp

		for zerologon_attack in result:

			attack = {}
			attack['Probability'] = zerologon_attack.pop('Probability')
			if '@timestamp0' in zerologon_attack: attack['Time'] = int(zerologon_attack.pop('@timestamp0'))
			else: attack['Time'] = int(zerologon_attack.pop('@timestamp'))
			attack['DetectionName'] = zerologon_attack.pop('DetectionName')

			try: attack['Details'] = zerologon_attack.pop('Details')
			except: pass

			attack['Target'] = zerologon_attack.pop('System.Computer')
			
			try: attack['Authenticator'] = zerologon_attack.pop('Transform.Authenticator')
			except: pass

			try: attack['TargetLogonId'] = zerologon_attack.pop('Data.TargetLogonId')
			except: pass
			try: attack['TargetDomain'] = zerologon_attack.pop('Data.TargetDomainName')
			except: pass
			try: attack['User'] = zerologon_attack.pop('Data.TargetUserName')
			except: pass
			try: attack['TargetSid'] = zerologon_attack.pop('Data.TargetSid')
			except: pass

			try: attack['PasswordLastSet'] = zerologon_attack.pop('Data.PasswordLastSet')
			except: pass
			try: attack['Raw Datas'] = zerologon_attack.pop('Data.RawData')
			except: pass

			suite = ''
			if 'Data.UtcTime_0' in zerologon_attack:
				i = 1
				i_saved = 0
				tmp_saved = zerologon_attack['Data.UtcTime_0']
				while 'Data.UtcTime_'+str(i) in zerologon_attack:
					if zerologon_attack['Data.UtcTime_'+str(i)] < tmp_saved:
						tmp_saved = zerologon_attack['Data.UtcTime_'+str(i)]
						i_saved = i
					i+=1
				suite = '_'+str(i_saved)

			try: attack['Host'] = zerologon_attack.pop('Data.SourceHostname'+suite)
			except: pass
			try: attack['DestinationIp'] = zerologon_attack.pop('Data.DestinationIp'+suite)
			except: pass
			try: attack['DestinationPort'] = zerologon_attack.pop('Data.DestinationPort#'+suite)
			except: pass
			try: attack['SourceIp'] = zerologon_attack.pop('Data.SourceIp'+suite)
			except: pass
			try: attack['SourcePort'] = zerologon_attack.pop('Data.SourcePort#'+suite)
			except: pass

			attacks.append(attack)

		return attacks



	def test_zerologon_attack(self, arg_index, MIN_PERCENT=None, VERBOSITY=1):
		if MIN_PERCENT is None: MIN_PERCENT=0.50
		
		if VERBOSITY > 0: print("[ ] Detection of Zerologon attack...")

		result = self._test_attack_detection_(arg_index, self.DETECTION_ZEROLOGON, PART_NAME="ZEROLOGON", VERBOSITY=VERBOSITY)
		
		attacks = self._traitment_datas_(result, VERBOSITY=VERBOSITY)

		### Deletion of low probability ###

		i = 0
		i_len = len(attacks)
		while i < i_len:
			attack = attacks[i]
			if attack['Probability'] < MIN_PERCENT:
				attacks.pop(i)
				i_len-=1
				continue
			i+=1

		### Sorting and display ###

		if len(attacks) > 0:
			if VERBOSITY > 0: print("[ ] Results : ")
			attacks = sorted(attacks, key=lambda attack: -1*attack['Probability'])

			i = 0
			i_len = len(attacks)
			while i < i_len:
				self._display_detection_list_(attacks[i])
				print('')
				i+=1
		else:
			if VERBOSITY > 0: print("[ ] No Zerologon attack detected !")

		return attacks