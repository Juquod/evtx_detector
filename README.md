![By cybex-assistance](./img/logo-cybex.png)



# evtx_detection

Displays a preliminary analysis by detecting certain attacks on a list of logs. 

:warning:  Logs need to be parsed by [EvtxToElk](https://gitlab.com/Juquod/evtxtoelk) !

Example of a PSExec detection :

![By cybex-assistance](./img/detection_psexec1.png)

### Install

To install every dependencies, you need to execute :

```sh
sudo python3 setup.py install
```

This library has been coded for Debian and works also on Ubuntu. Traduce the shell installer if you want to run it elsewhere.

### Launch

To see help, you can launch :

```sh
evtx_attacksdetection -h
```

To launch the treatment of the log files, you can execute the script like :

```sh
evtx_attacksdetection "my_elk_index" "my_second_index" ...
```

You can also export the result with the option `--csv` :

```sh
evtx_attacksdetection --csv "my_csv_file.csv" ...
```

