
import re

from attacks_detector import Attacks_Detector

class WINRM_Detector(Attacks_Detector):

	ELK_CONFIG = [{ 'host': 'localhost', 'port': 9200 }]

	def __init__(self):
		super().__init__(WINRM_Detector.ELK_CONFIG, "WINRM")

	##########################
	### DETECTION OF WINRM ###
	##########################


	DETECTION_WINRM = [
		{
			"Value": 4,
			"Optionnal": False,
			"Conditions": {
				"must": {
					"System.EventID#": 5,
					"System.ProviderName": "Microsoft-Windows-User Profiles Service"
				},
				"must_not": {
					"Data.Key.keyword": ".*_Classes",
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"System.Computer",
				"Data.File",
				"Data.Key"
			],
			"Transform": {
				"Data.LocalPath": { "Data.File": r"([cC]:\\.*)\\" },
				"Data.SubjectUserSid": { "Data.Key": r"(.*)" }
			},
			"Details": { "Success": "Registry file loaded (5)" }
		},
		{
			"Value": 1,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4672,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.SubjectUserSid": "Data.SubjectUserSid",
					"range_time": [ -200, 0 ]
				},
				"must_not": {
					"Data.SubjectUserName": "ANONYMOUS LOGON",
					"personalized": '{"query_string": {"fields": ["Data.SubjectUserName.keyword"],"query": "*$"} }'
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"Data.SubjectDomainName",
				"Data.SubjectLogonId",
				"Data.SubjectUserName",
				"Data.SubjectUserSid"
			],
			"Details": { "Success": "High privileges (4672)" }
		},
		{
			"Value": 1,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4624,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.LogonType#": 3,
					"Data.TargetLogonId": "Data.SubjectLogonId",
					"range_time": [ -20, 20 ]
				}
			},
			"Fields": [
				"Data.AuthenticationPackageName",
				"Data.ElevatedToken",
				"Data.IpAddress",
				"Data.IpPort#",
				"Data.WorkstationName"
			],
			"Details": { "Success": "Network connection (4624)" }
		},
		
		{
			"Value": 4,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 67,
					"System.ProviderName": "Microsoft-Windows-User Profiles Service",
					"System.Computer": "System.Computer",
					"Data.LocalPath": "Data.LocalPath",
					"range_time": [ 0, 200 ]
				}
			},
			"Fields": [
				"Data.ProfileType"
			],
			"Details": { "Success": "Logon informations (67)" }
		},
		{
			"Value": 4,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 5,
					"System.ProviderName": "Microsoft-Windows-User Profiles Service",
					"System.Computer": "System.Computer",
					"Data.Key": "{Data.SubjectUserSid}_Classes",
					"range_time": [ 0, 200 ]
				}
			},
			"Fields": [
				"Data.TargetUserName"
			],
			"Details": { "Success": "Registry file loaded (5)" }
		},
		{
			"Value": 3,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4799,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"range_time": [ 0, 300 ]
				}
			},
			"Details": { "Success": "Local group membership was enumerated (4799)" }
		},
		{
			"Value": 4,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 10016,
					"System.ProviderName": "DCOM",
					"System.Computer": "System.Computer",
					"range_time": [ 0, 300 ]
				},
				"should": {
					"Data.RawData.keyword": ".*{Data.SubjectUserSid}.*",
					"minimum_should_match": 1
				}
			},
			"Details": { "Success": "(10016)" }
		},
		{
			"Value": 4,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 91,
					"System.ProviderName": "Microsoft-Windows-WinRM",
					"System.Computer": "System.Computer",
					"System.SecurityUserID": "Data.SubjectUserSid",
					"ProcessingErrorData.DataItemName": "shellId",
					"range_time": [ 0, 500 ]
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"ProcessingErrorData.ErrorCode#",
				"ProcessingErrorData.EventPayload"
			],
			"Details": { "Success": "Session creation (91)" }
		},
		{
			"Value": 1,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 40961,
					"System.ProviderName": "Microsoft-Windows-PowerShell",
					"System.Computer": "System.Computer",
					"range_time": [ 0, 500 ]
				}
			},
			"Details": { "Success": "Engine state is changed from None to Available (40961)" }
		},
		{
			"Value": 5,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 53504,
					"System.ProviderName": "Microsoft-Windows-PowerShell",
					"System.Computer": "System.Computer",
					"range_time": [ 0, 1000 ]
				}
			},
			"Fields": [
				"Data.param1#",
				"Data.param2"
			],
			"Transform": {
				"Transform.IPCListeningProcess": { "Data.param1#": r"(.*)" },
				"Transform.IPCListeningAppDomain": { "Data.param2": r"(.*)" }
			},
			"Details": { "Success": "Windows PowerShell has started an IPC listening thread (53504)" }
		},
		{
			"Value": 4,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 400,
					"System.ProviderName": "PowerShell",
					"System.Computer": "System.Computer",
					"range_time": [ 0, 600 ]
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"Data.RawData"
			],
			"Transform": {
				"Transform.400StartState": { "Data.RawData": r"^.*?,(.*?)," },
				"Transform.400EndState": { "Data.RawData": r"^(.*?)," }
			},
			"Details": { "Success": "Engine state is being Available (400)" }
		},
		{
			"Value": 4,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 600,
					"System.ProviderName": "PowerShell",
					"System.Computer": "System.Computer",
					"range_time": [ -10, 50 ]
				}
			},
			"Details": { "Success": "Providers started (600)" }
		},
		{
			"Value": 1,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 40962,
					"System.ProviderName": "Microsoft-Windows-PowerShell",
					"System.Computer": "System.Computer",
					"range_time": [ 0, 500 ]
				}
			},
			"Details": { "Success": "PowerShell console is ready for user input (40962)" }
		},
		{
			"Value": 3,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4674,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.SubjectLogonId": "Data.SubjectLogonId",
					"Data.PrivilegeList": "*SeTakeOwnershipPrivilege*",
					"Data.SubjectDomainName": "Data.SubjectDomainName",
					"Data.SubjectUserSid": "Data.SubjectUserSid",
					"range_time": [ 0, 3600000 ]
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"Data.ProcessName"
			],
			"Details": { "Success": "Processes launched (4674)" }
		},
		{
			"Value": 0,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4634,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.TargetDomainName": "Data.TargetDomainName",
					"Data.TargetLogonId": "Data.TargetLogonId",
					"range_time": [ 0, 3600000 ]
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				}
			],
			"Details": { "Success": "End of session (4634)" }
		}
	]



	def _display_detection_list_(self, arg_detection):

		for key in arg_detection:
			if key == "Probability":
				print(" "+chr(0x21B3)+f"  Probability : {int(arg_detection[key]*100)} %")
			elif key == "Commands list":
				if len(arg_detection[key]) > 0:
					print(f"  | {key} :")
					for command in arg_detection[key]:
						print("    "+chr(0x21DB)+f" {self._date_from_timestamp_millis_(command['Time'])} - {command['Process']}")
			elif key == "Details":
				tmp = arg_detection[key].replace('\n', '\n    '+chr(0x21B3)+' ')
				print(f"  | {key} : {self.DETECTION_NAME} detection\n    "+chr(0x21B3)+f" {tmp}")
			elif "Time" in key or "time" in key:
				print(f"  | {key} : {self._date_from_timestamp_millis_(arg_detection[key])}")
			else:
				print(f"  | {key} : {arg_detection[key]}")


	def _test_attack_detection_(self, arg_index, arg_detection_list, PART_NAME="", VERBOSITY=1):

		if VERBOSITY > 0: print(f"[ ] Detection of WINRM usage ({PART_NAME})...")

		if isinstance(arg_index, list):
			result = []
			for index in arg_index:
				result_tmp = self._test_attack_(index, arg_detection_list)
				if not isinstance(result_tmp, int):
					for detection in result_tmp: result.append(detection)
		else: result = self._test_attack_(arg_index, arg_detection_list)
		if isinstance(result, int): result = []

		if isinstance(result, int):
			if result != 0: print(f'[-] Error during the detection of WINRM usage ({PART_NAME}) !')
		elif len(result) == 0:
			if VERBOSITY > 1: print(f"[+] Nothing detected ({PART_NAME}) !")
		elif VERBOSITY > 1:
			print(f"[+] Traces of WINRM usage detected ({PART_NAME}) !")
			if VERBOSITY > 2:
				print('')
				for detection in result:
					self._display_detection_list_(detection)
					print('')

		return result

	def _traitment_datas_(self, arg_result, VERBOSITY=1):

		if VERBOSITY > 0: print("[ ] Traitment of received datas...")
		
		REGEX_TIMESTAMP_DECOMP = re.compile("^@timestamp[0-9]*_([0-9]*)$")
		REGEX_KEY_NUMBER_DECOMP = re.compile("^(.*)_([0-9]*)$")
		
		attacks = []
		result = sorted(arg_result, key=lambda r: int(r['@timestamp']))	# Sort by timestamp

		for winrm_usage in result:

			attack = {}
			attack['Probability'] = winrm_usage.pop('Probability')
			if '@timestamp1' in winrm_usage:
				attack['Time'] = int(winrm_usage.pop('@timestamp1'))
				attack['EndTime'] = int(winrm_usage.pop('@timestamp'))
			else: attack['Time'] = int(winrm_usage.pop('@timestamp'))
			if "Console use" in winrm_usage['Details']: attack['DetectionName'] = "Console"
			else: attack['DetectionName'] = winrm_usage.pop('DetectionName')

			try: attack['Details'] = winrm_usage.pop('Details')
			except: pass

			attack['Target'] = winrm_usage.pop('System.Computer')

			try: attack['SubjectLogonId'] = winrm_usage.pop('Data.SubjectLogonId')
			except: pass

			try: attack['SubjectDomain'] = winrm_usage.pop('Data.SubjectDomainName')
			except: pass
			try: attack['SubjectUser'] = winrm_usage.pop('Data.SubjectUserName')
			except: pass
			try: 
				attack['SubjectUserSid'] = winrm_usage.pop('Data.SubjectUserSid')
				winrm_usage.pop('Data.Key')
			except: pass

			try: attack['ElevatedToken'] = winrm_usage.pop('Data.ElevatedToken')
			except: pass
			try: attack['ProfileType'] = winrm_usage.pop('Data.ProfileType')
			except: pass


			try: attack['AuthenticationPackage'] = winrm_usage.pop('Data.AuthenticationPackageName')
			except: pass
			try: attack['ProcessingErrorData.EventPayload'] = winrm_usage.pop('ProcessingErrorData.EventPayload')
			except: pass
			try: attack['ProcessingErrorData.ErrorCode#'] = winrm_usage.pop('ProcessingErrorData.ErrorCode#')
			except: pass
			try: attack['IPCListeningAppDomain'] = winrm_usage.pop('Transform.IPCListeningAppDomain')
			except: pass

			### Delete because no more usage ###
			try: winrm_usage.pop('@timestamp0')
			except: pass
			try: winrm_usage.pop('Data.RawData')
			except: pass
			try: winrm_usage.pop('Data.File')
			except: pass
			try: winrm_usage.pop('Data.LocalPath')
			except: pass
			try: winrm_usage.pop('Data.param1#')
			except: pass
			try: winrm_usage.pop('Data.param2')
			except: pass
			try: winrm_usage.pop('Transform.400StartState')
			except: pass
			try: winrm_usage.pop('Transform.400EndState')
			except: pass



			### Commands list ###

			l_timestamp_num = {}
			for key, value in winrm_usage.items():
				result = REGEX_TIMESTAMP_DECOMP.search(key)
				if result is not None:
					if result[1] not in l_timestamp_num: l_timestamp_num[result[1]] = {}
					l_timestamp_num[result[1]]['Time'] = value
				else:
					result = REGEX_KEY_NUMBER_DECOMP.search(key)
					if result is not None:
						if result[2] not in l_timestamp_num: l_timestamp_num[result[2]] = {}
						l_timestamp_num[result[2]][result[1]] = value

			l_process = []
			for key, value in l_timestamp_num.items():
				if 'Data.ProcessName' in value:
					if VERBOSITY < 2 and 'services.exe' in value['Data.ProcessName']: continue
					tmp = { 'Time': value['Time'] }
					tmp['Process'] = value['Data.ProcessName']
					l_process.append(tmp)
			l_process = sorted(l_process, key=lambda proc: proc['Time'])

			## Double suppression ##

			i_process = 1
			len_process = len(l_process)
			while i_process < len_process:
				if l_process[i_process]['Time'] == l_process[i_process-1]['Time'] and l_process[i_process]['Process'] == l_process[i_process-1]['Process']:
					l_process.pop(i_process)
					len_process -= 1
					continue
				i_process += 1

			attack['Commands list'] = l_process

			### ###

			attacks.append(attack)

		return attacks



	def test_winrm_use(self, arg_index, MIN_PERCENT=None, VERBOSITY=1):
		if MIN_PERCENT is None: MIN_PERCENT=0.4
		
		if VERBOSITY > 0: print("[ ] Detection of WINRM usage...")

		result = self._test_attack_detection_(arg_index, self.DETECTION_WINRM, PART_NAME="WINRM", VERBOSITY=VERBOSITY)
		attacks = self._traitment_datas_(result, VERBOSITY=VERBOSITY)

		### Deletion of low probability ###

		i = 0
		i_len = len(attacks)
		while i < i_len:
			attack = attacks[i]
			if attack['Probability'] < MIN_PERCENT:
				attacks.pop(i)
				i_len-=1
				continue
			i+=1

		### Deletion of console use into SMBEXEC detection ###

		attacks = sorted(attacks, key=lambda attack: attack['Time'])

		i = 0
		i_len = len(attacks)
		end_time = 0
		while i < i_len:
			attack = attacks[i]
			if 'EndTime' in attack:
				end_time = attack['EndTime']
				if i!=0 and attacks[i-1]['Time'] == attack['Time']:
					attacks.pop(i-1)
					i_len-=1
					continue
			else:
				if attack['Time'] <= end_time:
					attacks.pop(i)
					i_len-=1
					continue
			i+=1

		### Sorting and display ###

		if len(attacks) > 0:
			if VERBOSITY > 0: print("[ ] Results : ")
			attacks = sorted(attacks, key=lambda attack: -1*attack['Probability'])

			i = 0
			i_len = len(attacks)
			while i < i_len:
				self._display_detection_list_(attacks[i])
				print('')
				i+=1
		else:
			if VERBOSITY > 0: print("[ ] No usage of WINRM detected !")

		return attacks