#!/usr/bin/python3

# ./detection.py "brangeon_srvged" "brangeon_security" "brangeon_pcp_jdouge" "brangeon_srvadpom" -p 20
# ./detection.py "log_mimikatz_win7_2" "log_mimikatz_winserv_2" -p 31

import readchar
import sys

from detectors import LIST_REGISTRY_INFOS_Detector, PSEXEC_Detector, PTH_Detector, SMBEXEC_Detector, WINRM_Detector, ZEROLOGON_Detector

	#####################
	## Parameters PART ##
	#####################

INDEX_LIST = []
VERBOSITY = 1
MIN_PERCENT = None

CSV_FILE_PATH = ""
BOOL_CSV_EXPORT = False

argc = len(sys.argv)
if argc <= 1 :
	sys.stderr.write("[?] You should add '-h' argument to see help and usage of the script !\n")
	sys.exit(1)

i=1
while i < argc :

	if sys.argv[i] == "-h" :

		print("[?] -----------------> Attack detection <-----------------")
		print("  | ")
		print("  | Value waiting : ./script index_names [-h] [-q|-v|-vv] [-p <percent>] [--csv <output_csv_file>]")
		print("  | ")
		print("--| REQUIRED :")
		print("  | 	index_names \t The list of Elasticsearch index names into which the program will search")
		print("  | ")
		print("--| OPTIONAL :")
		print("  | 	--csv <output_csv_file> : Output result into a file with CSV format")
		print("  | 	-h \t Display the help and usage of the script")
		print("  | 	-p \t Minimum detection percent (0-100%) - Will display just suspects datas with a minimum correspondance.")
		print("  | 	-q \t Quiet mode : No verbosity.")
		print("  | 	-v \t Verbose mode : More verbosity than the normal. (Display complete details of search, etc.)")
		print("  | 	-vv \t Very Verbose mode : The most verbosity.")
		print("  | ")
		print("[?] -----------------> ****** ********* <-----------------")

		sys.exit(0)
	elif sys.argv[i] == "-p":
		if i+1 < argc :
			if sys.argv[i+1][0] == '-' :
				sys.stderr.write("[?] Form : -p <percent>\n")
				sys.exit(-1)

			try:
				tmp = float(sys.argv[i+1])
				if tmp > 1: MIN_PERCENT = tmp/100
				else: MIN_PERCENT = tmp
			except: pass

			i+=1

		else:
			sys.stderr.write("[?] Form : -p <percent>\n")
			sys.exit(-1)

	elif sys.argv[i] == "--csv":
		if i+1 < argc :
			if sys.argv[i+1][0] == '-' :
				sys.stderr.write("[?] Form : --csv <output_csv_file>\n")
				sys.exit(-1)

			CSV_FILE_PATH = sys.argv[i+1]
			BOOL_CSV_EXPORT = True

			i+=1

		else:
			sys.stderr.write("[?] Form : --csv <output_csv_file>\n")
			sys.exit(-1)


	elif sys.argv[i] == "-q": VERBOSITY = 0	# QUIET mode
	elif sys.argv[i] == "-v": VERBOSITY = 2	# VERBOSE mode
	elif sys.argv[i] == "-vv": VERBOSITY = 3	# VERY VERBOSE mode
	else: INDEX_LIST.append(sys.argv[i])

	i+=1

if VERBOSITY > 0:
	print(chr(0x2581)+chr(0x2581)+chr(0x2582)+chr(0x2582)+chr(0x2583)+chr(0x2583)+chr(0x2584)+chr(0x2584)+chr(0x2585)+chr(0x2585)+chr(0x2586)+chr(0x2586)+chr(0x2587)+chr(0x2587)+chr(0x2588)+chr(0x2588)+'  Attack detection  '+chr(0x2588)+chr(0x2588)+chr(0x2587)+chr(0x2587)+chr(0x2586)+chr(0x2586)+chr(0x2585)+chr(0x2585)+chr(0x2584)+chr(0x2584)+chr(0x2583)+chr(0x2583)+chr(0x2582)+chr(0x2582)+chr(0x2581)+chr(0x2581))
	print('     '+chr(0x2017)+chr(0x2017)+chr(0x2017)+chr(0x2017)+chr(0x2017))
	print('    '+chr(0x2571)+' '+chr(0x2017)+chr(0x2017)+chr(0x2017)+chr(0x2017)+chr(0x2572))
	print('   '+chr(0x2571)+' '+chr(0x2571)+'    '+chr(0x25BD))
	print('  '+chr(0x2595)+' '+chr(0x2595)+'  '+chr(0x205B)+' ybex Assistance - Célien Menneteau')
	print('   '+chr(0x2572)+' '+chr(0x2572)+chr(0x2017)+chr(0x2017)+chr(0x2017)+chr(0x2017)+chr(0x25B3))
	print('    '+chr(0x2572)+chr(0x2017)+chr(0x2017)+chr(0x2017)+chr(0x2017)+chr(0x2017)+chr(0x2571)+"\n")

	print(f" -> Verbosity : {VERBOSITY}")
	if MIN_PERCENT is not None: print(f" -> Minimum percent : {MIN_PERCENT*100}%")
	print(f" -> Index list : {INDEX_LIST[0]}", end="")
	i = 1
	while i < len(INDEX_LIST):
		print(f", {INDEX_LIST[i]}", end="")
		i+=1
	print('')
	if BOOL_CSV_EXPORT: print(f" -> Csv exportation file : {CSV_FILE_PATH}")

	print("--> Do you want to continue (Y/n) ? ")
	if readchar.readchar() == 'n':
		sys.exit(0)

attacks = []

my_Attacks_Detector = PTH_Detector()
attacks += my_Attacks_Detector.test_pth_attack(INDEX_LIST, MIN_PERCENT=MIN_PERCENT, VERBOSITY=VERBOSITY)
my_Attacks_Detector = PSEXEC_Detector()
attacks += my_Attacks_Detector.test_psexec_use(INDEX_LIST, MIN_PERCENT=MIN_PERCENT, VERBOSITY=VERBOSITY)
my_Attacks_Detector = SMBEXEC_Detector()
attacks += my_Attacks_Detector.test_smbexec_use(INDEX_LIST, MIN_PERCENT=MIN_PERCENT, VERBOSITY=VERBOSITY)
my_Attacks_Detector = ZEROLOGON_Detector()
attacks += my_Attacks_Detector.test_zerologon_attack(INDEX_LIST, MIN_PERCENT=MIN_PERCENT, VERBOSITY=VERBOSITY)
my_Attacks_Detector = LIST_REGISTRY_INFOS_Detector()
attacks += my_Attacks_Detector.test_list_registry_infos(INDEX_LIST, MIN_PERCENT=MIN_PERCENT, VERBOSITY=VERBOSITY)
my_Attacks_Detector = WINRM_Detector()
attacks += my_Attacks_Detector.test_winrm_use(INDEX_LIST, MIN_PERCENT=MIN_PERCENT, VERBOSITY=VERBOSITY)

if BOOL_CSV_EXPORT:
	csv = my_Attacks_Detector.to_csv(attacks)
	with open(CSV_FILE_PATH, 'w') as file:
		nb_line = file.write(csv)
		if VERBOSITY > 0:
			if nb_line: print("[+] Csv export done !")
			else: print('[-] Error during the csv export !')