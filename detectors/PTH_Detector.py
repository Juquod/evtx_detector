
from attacks_detector import Attacks_Detector

class PTH_Detector(Attacks_Detector):

	ELK_CONFIG = [{ 'host': 'localhost', 'port': 9200 }]

	def __init__(self):
		super().__init__(PTH_Detector.ELK_CONFIG, "Pass-The-Hash")

	####################################
	### DETECTION OF NTLM CONNECTION ###
	####################################

	DETECTION_PTH_TARGET_NTLM = [
		{
			"Value": 4,
			"Optionnal": False,
			"Conditions": {
				"must": {
					"System.EventID#": 4624,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"Data.LogonType#": 3,
					"Data.AuthenticationPackageName": "NTLM"
				},
				"must_not": {
					"Data.TargetUserName": "ANONYMOUS LOGON",
					"Data.TargetDomainName": "AUTORITE NT",
					"personalized": '{"query_string": {"fields": ["Data.TargetUserName.keyword"],"query": "*$"} }'
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"System.Computer",
				"Data.IpAddress",
				"Data.IpPort#",
				"Data.SubjectDomainName",
				"Data.SubjectLogonId",
				"Data.SubjectUserName",
				"Data.SubjectUserSid",
				"Data.TargetDomainName",
				"Data.TargetLogonId",
				"Data.TargetUserName",
				"Data.TargetUserSid",
				"Data.WorkstationName"
			],
			"Details": { "Success": "Target - NTLM network connection (4624)" }
		},
		{
			"Value": 8,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4672,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.SubjectLogonId": "Data.TargetLogonId",
					"range_time": [ -100, 100 ]
				}
			},
			"Details": {
				"Success": "Target - High privileges (4672)",
				"Fail": "Target - No privileges"
			}
		},
		{
			"Value": 1,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4674,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.SubjectLogonId": "Data.TargetLogonId",
					"range_time": [ -100, 100 ]
				}
			}
		}
	]

	DETECTION_PTH_DC_NTLM = [
		{
			"Value": 10,
			"Optionnal": False,
			"Conditions": {
				"must": {
					"System.EventID#": 4776,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing"
				},
				"must_not": {
					"personalized": '{"query_string": {"fields": ["Data.TargetUserName.keyword"],"query": "*$"} }'
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"System.Computer",
				"Data.PackageName",
				"Data.TargetUserName",
				"Data.Workstation"
			],
			"Details": { "Success": "DC - Validation of credentials via NTLM (4776)" }
		}
	]

	########################################
	### DETECTION OF KERBEROS CONNECTION ###
	########################################

	DETECTION_PTH_TARGET_KRBTGT = [
		{
			"Value": 4,
			"Optionnal": False,
			"Conditions": {
				"must": {
					"System.EventID#": 4624,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"Data.LogonType#": 3,
					"Data.AuthenticationPackageName": "Kerberos"
				},
				"must_not": {
					"Data.TargetUserName": "ANONYMOUS LOGON",
					"personalized": '{"query_string": {"fields": ["Data.TargetUserName.keyword"],"query": "*$"} }'
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"System.Computer",
				"Data.IpAddress",
				"Data.IpPort#",
				"Data.SubjectDomainName",
				"Data.SubjectLogonId",
				"Data.SubjectUserName",
				"Data.SubjectUserSid",
				"Data.TargetDomainName",
				"Data.TargetLogonId",
				"Data.TargetUserName",
				"Data.TargetUserSid",
				"Data.WorkstationName"
			],
			"Details": { "Success": "Target - Kerberos network connection (4624)" }
		},
		{
			"Value": 8,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4672,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.SubjectLogonId": "Data.TargetLogonId",
					"range_time": [ -100, 100 ]
				}
			},
			"Details": {
				"Success": "Target - High privileges (4672)",
				"Fail": "Target - No privileges"
			}
		}
	]

	DETECTION_PTH_DC_KRBTGT = [
		{
			"Value": 1,
			"Optionnal": False,
			"Conditions": {
				"must": {
					"System.EventID#": 4768,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"Data.Status": "0x0"
				},
				"must_not": {
					"personalized": '{"query_string": {"fields": ["Data.TargetUserName.keyword"],"query": "*$"} }'
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"System.Computer",
				"Data.IpAddress",
				"Data.IpPort#",
				"Data.PreAuthType#",
				"Data.TicketEncryptionType",
				"Data.TargetDomainName",
				"Data.TargetSid",
				"Data.TargetUserName",
				"Data.Workstation"
			],
			"Details": { "Success": "DC - Recovery of Kerberos ticket (4768)" }
		},
		{
			"Value": 1,
			"Optionnal": False,
			"Conditions": {
				"must": {
					"System.EventID#": 4769,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.TicketEncryptionType": "Data.TicketEncryptionType",
					"Data.IpAddress": "Data.IpAddress",
					"range_time": [ -5, 20 ]
				}
			},
			"Details": { "Success": "DC - A Kerberos service ticket was requested (4768)" }
		}
	]
	

	#############################
	### DETECTION OF MIMIKATZ ###
	#############################

	DETECTION_PTH_MIMIKATZ_HOST = [
		{
			"Value": 8,
			"Optionnal": False,
			"Conditions": {
				"must": {
					"System.EventID#": 4624,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"Data.LogonType#": 9,
					"Data.AuthenticationPackageName": "Negotiate",
					"Data.LogonProcessName": "seclogo"
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"System.Computer",
				"Data.IpAddress",
				"Data.IpPort#",
				"Data.SubjectDomainName",
				"Data.SubjectLogonId",
				"Data.SubjectUserName",
				"Data.SubjectUserSid",
				"Data.TargetDomainName",
				"Data.TargetLogonId",
				"Data.TargetUserName",
				"Data.TargetUserSid"
			],
			"Details": { "Success": "Mimikatz detection - Session 9 (4624)" }
		},
		{
			"Value": 8,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4672,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.SubjectLogonId": "Data.TargetLogonId",
					"range_time": [ -100, 100 ]
				}
			},
			"Fields": [ "Data.PrivilegeList" ],
			"Details": {
				"Success": "Host (mimikatz) - High privileges (4672)",
				"Fail": "Host - No privileges"
			}
		},
		{
			"Value": 1,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4656,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.SubjectLogonId": "Data.SubjectLogonId",
					"range_time": [ -100, 10000 ]
				}
			},
			"Details": { "Success": "Host (mimikatz) - Lsass.exe memory access (4656)" }
		},
		{
			"Value": 3,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4648,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.SubjectLogonId": "Data.TargetLogonId",
					"Data.TargetDomainName": "Data.TargetDomainName",
					"Data.TargetUserName": "Data.TargetUserName",
					"range_time": [ -100, 10000 ]
				}
			},
			"Details": { "Success": "Host (mimikatz) - A logon was attempted using explicit credentials (4648)" }
		}
	]


	def _test_attack_detection_(self, arg_index, arg_detection_list, PART_NAME="", VERBOSITY=1):

		if VERBOSITY > 0: print(f"[ ] Detection of Pass-the-Hash attack ({PART_NAME})...")

		if isinstance(arg_index, list):
			result = []
			for index in arg_index:
				result_tmp = self._test_attack_(index, arg_detection_list)
				if not isinstance(result_tmp, int):
					for detection in result_tmp: result.append(detection)
		else: result = self._test_attack_(arg_index, arg_detection_list)

		if isinstance(result, int):
			result = []
			if result != 0: print(f'[-] Error during the detection of Pass-the-Hash attack ({PART_NAME}) !')
		elif len(result) == 0:
			if VERBOSITY > 0: print(f"[+] Nothing detected ({PART_NAME}) !")
		elif VERBOSITY > 0:
			print(f"[+] Traces of Pass-the-Hash attack detected ({PART_NAME}) !")
			if VERBOSITY > 2:
				print('')
				for detection in result:
					self._display_detection_list_(detection)
					print('')

		return result


	def _traitment_datas_(self, arg_host, arg_target, arg_dc, time_diff_host_target_min=0, time_diff_host_target_max=60000, time_diff_target_dc_min=20, time_diff_target_dc_max=600000, host_rate=0.33, target_rate=0.33, dc_rate=0.34, VERBOSITY=1):

		if VERBOSITY > 0: print("[ ] Traitment of received datas...")
		
		attacks = []
		arg_host = sorted(arg_host, key=lambda host: int(host['@timestamp']))	# Sort host detection by timestamp

		### Associate host with target if possible ###

		for host in arg_host:

			attack = {}
			attack['Probability'] = host_rate * host['Probability']
			attack['Time'] = int(host['@timestamp'])
			attack['DetectionName'] = host['DetectionName']

			try: attack['Details'] = host['Details']
			except: pass

			attack['Host'] = host['System.Computer']
			attack['SourceIp'] = host['Data.IpAddress']
			attack['SourcePort'] = host['Data.IpPort#']

			attack['SubjectDomain'] = host['Data.SubjectDomainName']
			attack['SubjectUser'] = host['Data.SubjectUserName']
			attack['SubjectUserSid'] = host['Data.SubjectUserSid']
			attack['SubjectLogonId'] = host['Data.SubjectLogonId']

			attack['TargetDomain'] = host['Data.TargetDomainName']
			attack['TargetUser'] = host['Data.TargetUserName']
			attack['TargetUserSid'] = host['Data.TargetUserSid']
			attack['TargetLogonId'] = host['Data.TargetLogonId']

			min_time = attack['Time'] - time_diff_host_target_min
			max_time = min_time + time_diff_host_target_max
			i_min = -1
			value_min = -1

			i = 0
			i_len = len(arg_target)
			while i < i_len:
				target = arg_target[i]
				tmp_time = int(target['@timestamp'])
				if tmp_time >= min_time and tmp_time <= max_time:
					if i_min == -1 or value_min > tmp_time:
						if attack['TargetUserSid'] == target['Data.TargetUserSid']:
							i_min = i
							value_min = tmp_time
				i+=1

			if i_min != -1:
				target = arg_target.pop(i_min)
				attack['Probability'] += target_rate * target['Probability']
				try: 
					if 'Details' in attack: attack['Details'] += ',\n'+target['Details']
					else: attack['Details'] = target['Details']
				except: pass
				attack['Target'] = target['System.Computer']
				try:
					attack['SourceIp'] = target['Data.IpAddress']
					attack['SourcePort'] = target['Data.IpPort#']
				except: pass
				try: attack['Workstation'] = target['Data.Workstation']
				except: 
					try: attack['Workstation'] = target['Data.WorkstationName']
					except: pass

			attacks.append(attack)

		### Create attack group with targets detection not already associated ###

		i = 0
		i_len = len(arg_target)
		while i < i_len:
			target = arg_target[i]
			attack = {}
			attack['Probability'] = target_rate * target['Probability']
			attack['Time'] = int(target['@timestamp'])
			attack['DetectionName'] = target['DetectionName']

			try: attack['Details'] = target['Details']
			except: pass

			attack['Target'] = target['System.Computer']
			try: attack['SourceIp'] = target['Data.IpAddress']
			except: pass
			try: attack['SourcePort'] = target['Data.IpPort#']
			except: pass

			try: attack['SubjectDomain'] = target['Data.SubjectDomainName']
			except: pass
			try: attack['SubjectUser'] = target['Data.SubjectUserName']
			except: pass
			attack['SubjectUserSid'] = target['Data.SubjectUserSid']
			attack['SubjectLogonId'] = target['Data.SubjectLogonId']

			attack['TargetDomain'] = target['Data.TargetDomainName']
			attack['TargetUser'] = target['Data.TargetUserName']
			attack['TargetUserSid'] = target['Data.TargetUserSid']
			attack['TargetLogonId'] = target['Data.TargetLogonId']

			try: attack['Workstation'] = target['Data.Workstation']
			except: 
				try: attack['Workstation'] = target['Data.WorkstationName']
				except: pass

			attacks.append(attack)
			i+=1

		attacks = sorted(attacks, key=lambda attack: int(attack['Time']))

		### Associate attacks with DC detection ###

		for attack in attacks:

			min_time = attack['Time'] - time_diff_target_dc_min
			max_time = min_time + time_diff_target_dc_max
			i_min = -1
			value_min = -1

			i = 0
			i_len = len(arg_dc)
			while i < i_len:
				dc = arg_dc[i]
				tmp_time = int(dc['@timestamp'])
				if tmp_time >= min_time and tmp_time <= max_time:
					if i_min == -1 or value_min > tmp_time:
						workstation = ""
						if 'Data.Workstation' in dc : workstation = dc['Data.Workstation']
						elif 'Data.WorkstationName' in dc : workstation = dc['Data.WorkstationName']
						if workstation == "" or ( 'Workstation' in attack and attack['Workstation'] == workstation ):
							i_min = i
							value_min = tmp_time
				i+=1

			if i_min != -1:
				dc = arg_dc.pop(i_min)
				attack['Probability'] += dc_rate * dc['Probability']

				try: 
					if 'Details' in attack: attack['Details'] += ',\n'+dc['Details']
					else: attack['Details'] = dc['Details']
				except: pass
				attack['DC'] = dc['System.Computer']
				try:
					attack['SourceIp'] = dc['Data.IpAddress']
					attack['SourcePort'] = dc['Data.IpPort#']
				except: pass

		### Create attack group with dc detection not already associated ###

		i = 0
		i_len = len(arg_dc)
		while i < i_len:
			dc = arg_dc[i]
			attack = {}
			attack['Probability'] = dc_rate * dc['Probability']
			attack['Time'] = int(dc['@timestamp'])
			attack['DetectionName'] = dc['DetectionName']

			try: attack['Details'] = dc['Details']
			except: pass

			attack['DC'] = dc['System.Computer']
			try: attack['TargetDomain'] = dc['Data.TargetDomainName']
			except: pass
			try: attack['TargetSid'] = dc['Data.TargetSid']
			except: pass
			attack['TargetUser'] = dc['Data.TargetUserName']

			try: attack['SourceIp'] = dc['Data.IpAddress']
			except: pass
			try: attack['SourcePort'] = dc['Data.IpPort#']
			except: pass
			try: attack['Workstation'] = dc['Data.Workstation']
			except: 
				try: attack['Workstation'] = dc['Data.WorkstationName']
				except: pass
			try: attack['TicketEncryptionType'] = dc['Data.TicketEncryptionType']
			except: pass

			attacks.append(attack)
			i+=1

		return attacks
		


	def _display_detection_list_(self, arg_detection):

		for key in arg_detection:
			if key == "Probability":
				print(" "+chr(0x21B3)+f"  Probability : {int(arg_detection[key]*100)} %")
			elif key == "Privileges":
				print(f"  | {key} :")
				for word in arg_detection[key].split():
					print("    "+chr(0x21DB)+f" {word}")
			elif key == "Details":
				tmp = arg_detection[key].replace('\n', '\n    '+chr(0x21B3)+' ')
				print(f"  | {key} : {self.DETECTION_NAME} detection\n    "+chr(0x21B3)+f" {tmp}")
			elif "Time" in key or "time" in key:
				print(f"  | {key} : {self._date_from_timestamp_millis_(arg_detection[key])}")
			else:
				print(f"  | {key} : {arg_detection[key]}")


	def test_pth_attack(self, arg_index, MIN_PERCENT=None, VERBOSITY=1):
		if MIN_PERCENT is None: MIN_PERCENT=0.50
		
		if VERBOSITY > 0: print("[ ] Detection of Pass-the-Hash attack...")

		result_host = self._test_attack_detection_(arg_index, self.DETECTION_PTH_MIMIKATZ_HOST, PART_NAME="HOST PART - MIMIKATZ", VERBOSITY=VERBOSITY)
		result_target_ntlm = self._test_attack_detection_(arg_index, self.DETECTION_PTH_TARGET_NTLM, PART_NAME="TARGET PART - NTLM", VERBOSITY=VERBOSITY)
		result_target_krbtgt = self._test_attack_detection_(arg_index, self.DETECTION_PTH_TARGET_KRBTGT, PART_NAME="TARGET PART - KERBEROS", VERBOSITY=VERBOSITY)
		result_dc_ntlm = self._test_attack_detection_(arg_index, self.DETECTION_PTH_DC_NTLM, PART_NAME="DC PART - NTLM", VERBOSITY=VERBOSITY)
		result_dc_krbtgt = self._test_attack_detection_(arg_index, self.DETECTION_PTH_DC_KRBTGT, PART_NAME="DC PART - KERBEROS", VERBOSITY=VERBOSITY)
		
		attacks = self._traitment_datas_(result_host, result_target_ntlm, result_dc_ntlm, time_diff_host_target_min=0, time_diff_host_target_max=60000, time_diff_target_dc_min=20, time_diff_target_dc_max=600000, host_rate=0.3, target_rate=0.4, dc_rate=0.3, VERBOSITY=VERBOSITY)
		attacks += self._traitment_datas_(result_host, result_target_krbtgt, result_dc_krbtgt, time_diff_host_target_min=0, time_diff_host_target_max=60000, time_diff_target_dc_min=500, time_diff_target_dc_max=60000, host_rate=0.6, target_rate=0.2, dc_rate=0.2, VERBOSITY=VERBOSITY)

		### Deletion of low probability ###

		i = 0
		i_len = len(attacks)
		while i < i_len:
			attack = attacks[i]
			if attack['Probability'] < MIN_PERCENT:
				attacks.pop(i)
				i_len-=1
				continue
			i+=1

		### Suppression of doubles ###

		i = 0
		i_len = len(attacks)
		while i < i_len:

			t = attacks[i]['Time']
			j = i+1
			while j < i_len:
				if attacks[j]['Time'] == t:
					if attacks[i]['Probability'] > attacks[j]['Probability']:
						attacks.pop(j)
						i_len-=1
						continue
					else:
						attacks.pop(i)
						i-=1
						i_len-=1
						break
				j+=1
			i+=1

		### Sorting and display ###

		if len(attacks) > 0:
			if VERBOSITY > 0: print("[ ] Results : ")
			attacks = sorted(attacks, key=lambda attack: -1*attack['Probability'])

			i = 0
			i_len = len(attacks)
			while i < i_len:
				self._display_detection_list_(attacks[i])
				print('')
				i+=1
		else:
			if VERBOSITY > 0: print("[ ] No Pass-the-Hash attack detected !")

		return attacks