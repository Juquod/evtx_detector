
import re

from attacks_detector import Attacks_Detector

class SMBEXEC_Detector(Attacks_Detector):

	ELK_CONFIG = [{ 'host': 'localhost', 'port': 9200 }]

	def __init__(self):
		super().__init__(SMBEXEC_Detector.ELK_CONFIG, "SMBExec")

	############################
	### DETECTION OF SMBEXEC ###
	############################


	DETECTION_SMBEXEC = [
		{
			"Value": 10,
			"Optionnal": False,
			"Conditions": {
				"must": {
					"System.EventID#": 7045,
					"System.ProviderName": "Service Control Manager"
				},
				"should": {
					"Data.RawData.keyword": ".*BTOBTO.*%COMSPEC% /Q /c echo cd +^.gt; *\\\\\\\\127.0.0.1\\\\C$\\\\__output 2^.gt;^.amp;1 .gt; %TEMP%\\\\execute.bat .amp; %COMSPEC% /Q /c %TEMP%\\\\execute.bat .amp; del %TEMP%\\\\execute.bat.*",
					"Data.ImagePath.keyword": ".*%COMSPEC% /Q /c echo cd +^.gt; *\\\\\\\\127.0.0.1\\\\C$\\\\__output 2^.gt;^.amp;1 .gt; %TEMP%\\\\execute.bat .amp; %COMSPEC% /Q /c %TEMP%\\\\execute.bat .amp; del %TEMP%\\\\execute.bat.*",
					"minimum_should_match": 1
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"System.Computer",
				"Data.ServiceType",
				"Data.ServiceName",
				"Data.RawData"
			],
			"Transform": {
				"Data.ServiceName": {
					"Data.RawData": r"^(.+?),.*"
				}
			},
			"Details": { "Success": "BTOBTO Service installed (7045)" }
		},
		{
			"Value": 4,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4776,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"range_time": [ -400, 400 ]
				},
				"must_not": {
					"personalized": '{"query_string": {"fields": ["Data.TargetUserName.keyword"],"query": "*$"} }'
				}
			},
			"Fields": [
				"Data.TargetUserName",
				"Data.Workstation"
			],
			"Details": { "Success": "Validation des informations d’identification (4776)" }
		},
		{
			"Value": 8,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4624,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"Data.LogonType#": 3,
					"Data.AuthenticationPackageName": "NTLM",
					"range_time": [ -500, 500 ]
				},
				"must_not": {
					"Data.TargetUserName": "ANONYMOUS LOGON",
					"personalized": '{"query_string": {"fields": ["Data.TargetUserName.keyword"],"query": "*$"} }'
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"Data.IpAddress",
				"Data.IpPort#",
				"Data.TargetDomainName",
				"Data.TargetLogonId",
				"Data.TargetUserName",
				"Data.TargetUserSid",
				"Data.WorkstationName"
			],
			"Details": { "Success": "NTLM network session start (4624)" }
		},
		{
			"Value": 4,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4672,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.SubjectLogonId": "Data.TargetLogonId",
					"range_time": [ -10, 200 ]
				}
			},
			"Details": {
				"Success": "High privileges (4672)",
				"Fail": "No privileges"
			}
		},
		{
			"Value": 5,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 7045,
					"System.ProviderName": "Service Control Manager",
					"System.Computer": "System.Computer",
					"range_time": [ -10, 3600000 ]
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"Data.RawData"
			],
			"Details": { "Success": "Services launched (7045)" }
		},
		{
			"Value": 2,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4634,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.TargetDomainName": "Data.TargetDomainName",
					"Data.TargetLogonId": "Data.TargetLogonId",
					"range_time": [ -3600000, 3600000 ]
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				}
			],
			"Details": { "Success": "End of session (4634)" }
		}
	]


	DETECTION_CONSOLE_USE = [
		{
			"Value": 10,
			"Optionnal": False,
			"Conditions": {
				"must": {
					"System.EventID#": 7045,
					"System.ProviderName": "Service Control Manager"
				},
				"should": {
					"Data.RawData": "*%COMSPEC%*",
					"Data.ImagePath": "*%COMSPEC%*",
					"minimum_should_match": 1
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"System.Computer",
				"Data.ServiceType",
				"Data.ServiceName",
				"Data.ImagePath",
				"Data.RawData"
			],
			"Transform": {
				"Data.ServiceName": {
					"Data.RawData": r"^(.+?),.*"
				},
				"Data.ImagePath": {
					"Data.RawData": r"^.*?,((?:\".*,.*\"|.?)*?),"
				}
			},
			"Details": { "Success": "Console use (7045)" }
		},
		{
			"Value": 0,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 7009,
					"System.ProviderName": "Service Control Manager",
					"Data.param2": "Data.ServiceName",
					"range_time": [ 0, 5000 ]
				}
			},
			"Details": { "Success": "Timeout waiting for the service to connect (7009)" }
		}
	]


	def _display_detection_list_(self, arg_detection):

		for key in arg_detection:
			if key == "Probability":
				print(" "+chr(0x21B3)+f"  Probability : {int(arg_detection[key]*100)} %")
			elif key == "Commands list":
				if len(arg_detection[key]) > 0:
					print(f"  | {key} :")
					for command in arg_detection[key]:
						print("    "+chr(0x21DB)+f" {self._date_from_timestamp_millis_(command['Time'])} - {command['Command']}")
			elif key == "Details":
				tmp = arg_detection[key].replace('\n', '\n    '+chr(0x21B3)+' ')
				print(f"  | {key} : {self.DETECTION_NAME} detection\n    "+chr(0x21B3)+f" {tmp}")
			elif "Time" in key or "time" in key:
				print(f"  | {key} : {self._date_from_timestamp_millis_(arg_detection[key])}")
			else:
				print(f"  | {key} : {arg_detection[key]}")


	def _test_attack_detection_(self, arg_index, arg_detection_list, PART_NAME="", VERBOSITY=1):

		if VERBOSITY > 0: print(f"[ ] Detection of SMBExec usage ({PART_NAME})...")

		if isinstance(arg_index, list):
			result = []
			for index in arg_index:
				result_tmp = self._test_attack_(index, arg_detection_list)
				if not isinstance(result_tmp, int):
					for detection in result_tmp: result.append(detection)
		else: result = self._test_attack_(arg_index, arg_detection_list)
		if isinstance(result, int): result = []

		if isinstance(result, int):
			if result != 0: print(f'[-] Error during the detection of SMBExec usage ({PART_NAME}) !')
		elif len(result) == 0:
			if VERBOSITY > 1: print(f"[+] Nothing detected ({PART_NAME}) !")
		elif VERBOSITY > 1:
			print(f"[+] Traces of SMBExec usage detected ({PART_NAME}) !")
			if VERBOSITY > 2:
				print('')
				for detection in result:
					self._display_detection_list_(detection)
					print('')

		return result

	def _traitment_datas_(self, arg_result, VERBOSITY=1):

		if VERBOSITY > 0: print("[ ] Traitment of received datas...")
		
		REGEX_TIMESTAMP_DECOMP = re.compile("^@timestamp[0-9]*_([0-9]*)$")
		REGEX_KEY_BUMBER_DECOMP = re.compile("^(.*)_([0-9]*)$")
		REGEX_GET_COMMANDE = re.compile('%COMSPEC% \/Q \/c echo (.*?) *(?=\^&gt; \\\\\\\\127\.0\.0\.1\\\\C\$\\\\__output)')
		
		attacks = []
		result = sorted(arg_result, key=lambda r: int(r['@timestamp']))	# Sort by timestamp

		for smbexec_usage in result:

			attack = {}
			attack['Probability'] = smbexec_usage.pop('Probability')
			if '@timestamp0' in smbexec_usage:
				attack['Time'] = int(smbexec_usage.pop('@timestamp0'))
				attack['EndTime'] = int(smbexec_usage.pop('@timestamp'))
			else: attack['Time'] = int(smbexec_usage.pop('@timestamp'))
			if "Console use" in smbexec_usage['Details']: attack['DetectionName'] = "Console"
			else: attack['DetectionName'] = smbexec_usage.pop('DetectionName')

			try: attack['Details'] = smbexec_usage.pop('Details')
			except: pass

			attack['Host'] = smbexec_usage.pop('System.Computer')

			try: attack['Service'] = smbexec_usage.pop('Data.ServiceName')
			except: pass
			try: attack['ServiceType'] = smbexec_usage.pop('Data.ServiceType')
			except: pass

			try: attack['SourceIp'] = smbexec_usage.pop('Data.IpAddress')
			except: pass
			try: attack['SourcePort'] = smbexec_usage.pop('Data.IpPort#')
			except: pass

			try: attack['TargetLogonId'] = smbexec_usage.pop('Data.TargetLogonId')
			except: pass

			try: attack['TargetDomain'] = smbexec_usage.pop('Data.TargetDomainName')
			except: pass
			try: attack['TargetUser'] = smbexec_usage.pop('Data.TargetUserName')
			except: pass
			try: attack['TargetUserSid'] = smbexec_usage.pop('Data.TargetUserSid')
			except: pass

			try: attack['Command'] = smbexec_usage.pop('Data.ImagePath')
			except: pass

			### Commands list ###

			min_time=attack['Time']
			if 'EndTime' in smbexec_usage: max_time = attack['EndTime']
			else : max_time = None

			l_timestamp_num = {}
			for key, value in smbexec_usage.items():
				result = REGEX_TIMESTAMP_DECOMP.search(key)
				if result is not None:
					if result[1] not in l_timestamp_num: l_timestamp_num[result[1]] = {}
					l_timestamp_num[result[1]]['Time'] = value
				else:
					result = REGEX_KEY_BUMBER_DECOMP.search(key)
					if result is not None:
						if result[2] not in l_timestamp_num: l_timestamp_num[result[2]] = {}
						l_timestamp_num[result[2]][result[1]] = value
			
			l_commandes = []
			for key, value in l_timestamp_num.items():
				
				result = REGEX_GET_COMMANDE.search(value['Data.RawData'])
				if result is not None:
					tmp = { 'Time': int(value['Time']) }
					tmp['Command'] = result[1]
				l_commandes.append(tmp)
			l_commandes = sorted(l_commandes, key=lambda comm: comm['Time'])

			## Double suppression ##

			i_command = 1
			len_commands = len(l_commandes)
			while i_command < len_commands:
				if ( l_commandes[i_command]['Time'] == l_commandes[i_command-1]['Time'] and l_commandes[i_command]['Command'] == l_commandes[i_command-1]['Command'] ) or ( "(4634)" in attack['Details'] and l_commandes[i_command]['Time'] > attack['EndTime'] ) :
					l_commandes.pop(i_command)
					len_commands -= 1
					continue
				i_command += 1

			### Update of attack information list ###

			if len(l_commandes) > 0:
				attack['Commands list'] = l_commandes
				for command in l_commandes:
					try:
						if command['Time'] > attack['EndTime']: attack['EndTime'] = command['Time']
					except:
						if command['Time'] > attack['Time']: attack['EndTime'] = int(command['Time'])

			attacks.append(attack)

		return attacks



	def test_smbexec_use(self, arg_index, MIN_PERCENT=None, VERBOSITY=1):
		if MIN_PERCENT is None: MIN_PERCENT=0.25
		
		if VERBOSITY > 0: print("[ ] Detection of SMBExec usage...")

		result = self._test_attack_detection_(arg_index, self.DETECTION_SMBEXEC, PART_NAME="SMBEXEC", VERBOSITY=VERBOSITY)
		result += self._test_attack_detection_(arg_index, self.DETECTION_CONSOLE_USE, PART_NAME="Console USE", VERBOSITY=VERBOSITY)
		
		attacks = self._traitment_datas_(result, VERBOSITY=VERBOSITY)

		### Deletion of low probability ###

		i = 0
		i_len = len(attacks)
		while i < i_len:
			attack = attacks[i]
			if attack['Probability'] < MIN_PERCENT:
				attacks.pop(i)
				i_len-=1
				continue
			i+=1

		### Deletion of console use into SMBEXEC detection ###

		attacks = sorted(attacks, key=lambda attack: attack['Time'])

		i = 0
		i_len = len(attacks)
		end_time = 0
		while i < i_len:
			attack = attacks[i]
			if 'EndTime' in attack:
				end_time = attack['EndTime']
				if i!=0 and attacks[i-1]['Time'] == attack['Time']:
					attacks.pop(i-1)
					i_len-=1
					continue
			else:
				if attack['Time'] <= end_time:
					attacks.pop(i)
					i_len-=1
					continue
			i+=1

		### Sorting and display ###

		if len(attacks) > 0:
			if VERBOSITY > 0: print("[ ] Results : ")
			attacks = sorted(attacks, key=lambda attack: -1*attack['Probability'])

			i = 0
			i_len = len(attacks)
			while i < i_len:
				self._display_detection_list_(attacks[i])
				print('')
				i+=1
		else:
			if VERBOSITY > 0: print("[ ] No usage of SMBExec detected !")

		return attacks