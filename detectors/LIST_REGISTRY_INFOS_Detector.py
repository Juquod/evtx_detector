
import re

from attacks_detector import Attacks_Detector

class LIST_REGISTRY_INFOS_Detector(Attacks_Detector):

	ELK_CONFIG = [{ 'host': 'localhost', 'port': 9200 }]

	def __init__(self):
		super().__init__(LIST_REGISTRY_INFOS_Detector.ELK_CONFIG, "Registry listing")

	###########################
	### DETECTION OF PSEXEC ###
	###########################


	DETECTION_LIST_REGISTRY_INFOS = [
		{
			"Value": 1,
			"Optionnal": False,
			"Conditions": {
				"must": {
					"System.EventID#": 4672,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing"
				},
				"must_not": {
					"Data.SubjectUserName": "ANONYMOUS LOGON",
					"personalized": '{"query_string": {"fields": ["Data.SubjectUserName.keyword"],"query": "*$"} }'
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"System.Computer",
				"Data.SubjectDomainName",
				"Data.SubjectLogonId",
				"Data.SubjectUserName",
				"Data.SubjectUserSid"
			],
			"Details": { "Success": "High privileges (4672)" }
		},
		{
			"Value": 1,
			"Optionnal": False,
			"Conditions": {
				"must": {
					"System.EventID#": 4624,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.LogonType#": 3,
					"Data.TargetLogonId": "Data.SubjectLogonId",
					"range_time": [ -5, 5 ]
				}
			},
			"Fields": [
				"Data.IpAddress",
				"Data.IpPort#",
				"Data.WorkstationName"
			],
			"Details": { "Success": "Network connection (4624)" }
		},
		{
			"Value": 5,
			"Optionnal": False,
			"Conditions": {
				"must": {
					"System.EventID#": 4799,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.SubjectLogonId": "Data.SubjectLogonId",
					"Data.SubjectUserSid": "Data.SubjectUserSid",
					"range_time": [ 0, 3600000 ]
				}
			},
			"Fields": [
				"Data.TargetDomainName",
				"Data.TargetSid",
				"Data.TargetUserName"
			],
			"Details": { "Success": "Local group membership was enumerated (4799)" }
		}
	]



	def _display_detection_list_(self, arg_detection):

		for key in arg_detection:
			if key == "Probability":
				print(" "+chr(0x21B3)+f"  Probability : {int(arg_detection[key]*100)} %")
			elif key == "Details":
				tmp = arg_detection[key].replace('\n', '\n    '+chr(0x21B3)+' ')
				print(f"  | {key} : {self.DETECTION_NAME} detection\n    "+chr(0x21B3)+f" {tmp}")
			elif key == "Local group membership listing":
				if len(arg_detection[key]) > 0:
					print(f"  | Local group membership listing :")
					for listing in arg_detection[key]:
						print("    "+chr(0x21DB)+f" {listing['Data.TargetSid']} - {listing['Data.TargetDomainName']}\\{listing['Data.TargetUserName']}")
			elif "Time" in key or "time" in key:
				print(f"  | {key} : {self._date_from_timestamp_millis_(arg_detection[key])}")
			else:
				print(f"  | {key} : {arg_detection[key]}")


	def _test_attack_detection_(self, arg_index, arg_detection_list, PART_NAME="", VERBOSITY=1):

		if VERBOSITY > 0: print(f"[ ] Detection of registry infos listing ({PART_NAME})...")

		if isinstance(arg_index, list):
			result = []
			for index in arg_index:
				result_tmp = self._test_attack_(index, arg_detection_list)
				if not isinstance(result_tmp, int):
					for detection in result_tmp: result.append(detection)
		else: result = self._test_attack_(arg_index, arg_detection_list)
		if isinstance(result, int): result = []

		if isinstance(result, int):
			if result != 0: print(f'[-] Error during the detection of registry infos listing ({PART_NAME}) !')
		elif len(result) == 0:
			if VERBOSITY > 1: print(f"[+] Nothing detected ({PART_NAME}) !")
		elif VERBOSITY > 1:
			print(f"[+] Traces of registry infos listing detected ({PART_NAME}) !")
			if VERBOSITY > 2:
				print('')
				for detection in result:
					self._display_detection_list_(detection)
					print('')

		return result

	def _traitment_datas_(self, arg_result, VERBOSITY=1):

		if VERBOSITY > 0: print("[ ] Traitment of received datas...")
		
		listing = []
		regex_key_number_decomp = re.compile("^(.*)_([0-9]*)$")
		result = sorted(arg_result, key=lambda r: int(r['@timestamp']))	# Sort by timestamp

		for registry_listing in result:

			attack = {}
			attack['Probability'] = registry_listing.pop('Probability')
			if '@timestamp0' in registry_listing: attack['Time'] = int(registry_listing.pop('@timestamp0'))
			else: attack['Time'] = int(registry_listing.pop('@timestamp'))
			attack['DetectionName'] = registry_listing.pop('DetectionName')

			try: attack['Details'] = registry_listing.pop('Details')
			except: pass

			attack['Target'] = registry_listing.pop('System.Computer')

			try: attack['DestinationIp'] = registry_listing.pop('Data.DestinationIp'+suite)
			except: pass
			try: attack['DestinationPort'] = registry_listing.pop('Data.DestinationPort#'+suite)
			except: pass
			try: attack['SourceIp'] = registry_listing.pop('Data.IpAddress')
			except: pass
			try: attack['SourcePort'] = registry_listing.pop('Data.IpPort#')
			except: pass
			try: attack['Workstation'] = registry_listing.pop('Data.WorkstationName')
			except: pass

			try: attack['SubjectLogonId'] = registry_listing.pop('Data.SubjectLogonId')
			except: pass
			try: attack['SubjectDomain'] = registry_listing.pop('Data.SubjectDomainName')
			except: pass
			try: attack['SubjectUser'] = registry_listing.pop('Data.SubjectUserName')
			except: pass
			try: attack['SubjectSid'] = registry_listing.pop('Data.SubjectUserSid')
			except: pass

			### Local group membership listing ###

			l_num = {}
			for key, value in registry_listing.items():
				result = regex_key_number_decomp.search(key)
				if result is not None:
					if result[2] not in l_num: l_num[result[2]] = {}
					l_num[result[2]][result[1]] = value

			if len(l_num) > 0: attack['Local group membership listing'] = list(l_num.values())
			else:
				try: attack['Local group membership listing'] = [{'Data.TargetSid': registry_listing.pop('Data.TargetSid'), 'Data.TargetDomainName': registry_listing.pop('Data.TargetDomainName'), 'Data.TargetUserName': registry_listing.pop('Data.TargetUserName')}]
				except: pass

			### ###

			listing.append(attack)

		return listing



	def test_list_registry_infos(self, arg_index, MIN_PERCENT=None, VERBOSITY=1):
		if MIN_PERCENT is None: MIN_PERCENT=1.0
		
		if VERBOSITY > 0: print("[ ] Detection of registry infos listing...")

		result = self._test_attack_detection_(arg_index, self.DETECTION_LIST_REGISTRY_INFOS, PART_NAME="LISTING REGISTRY INFOS", VERBOSITY=VERBOSITY)
		
		attacks = self._traitment_datas_(result, VERBOSITY=VERBOSITY)

		### Deletion of low probability ###

		i = 0
		i_len = len(attacks)
		while i < i_len:
			attack = attacks[i]
			if attack['Probability'] < MIN_PERCENT:
				attacks.pop(i)
				i_len-=1
				continue
			i+=1

		### Sorting and display ###

		if len(attacks) > 0:
			if VERBOSITY > 0: print("[ ] Results : ")
			attacks = sorted(attacks, key=lambda attack: -1*attack['Probability'])

			i = 0
			i_len = len(attacks)
			while i < i_len:
				self._display_detection_list_(attacks[i])
				print('')
				i+=1
		else:
			if VERBOSITY > 0: print("[ ] No registry infos listing detected !")

		return attacks