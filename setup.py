import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="cm_attacks_detector",
    version="0.9.0",
    author="Célien Menneteau",
    author_email="celien@cybex-assistance.com",
    description="Tool which try to detect attacks from evtx log",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Juquod/evtxtoelk",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: Apache 2.0 Licence",
        "Operating System :: Unix",
    ],
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
    install_requires=[
        'datetime',
        'elasticsearch',
        'readchar'
    ],
    scripts=[
        "scripts/evtx_attacksdetection"
    ]
)