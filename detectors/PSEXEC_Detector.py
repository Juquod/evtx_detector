
import re

from attacks_detector import Attacks_Detector

class PSEXEC_Detector(Attacks_Detector):

	ELK_CONFIG = [{ 'host': 'localhost', 'port': 9200 }]

	def __init__(self):
		super().__init__(PSEXEC_Detector.ELK_CONFIG, "PSExec")

	###########################
	### DETECTION OF PSEXEC ###
	###########################


	DETECTION_PSEXEC = [
		{
			"Value": 1,
			"Optionnal": False,
			"Conditions": {
				"must": {
					"System.EventID#": 7036,
					"System.ProviderName": "Service Control Manager"
				},
				"should": [
					{ "Data.Binary.keyword": ".*4000000" },	# Launch of the service
					{ "minimum_should_match": 1 }
				]
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"System.Computer",
				"Data.param1",
				"Data.RawData"
			],
			"Transform": {
				"Transform.ServiceName": {
					"Data.RawData": r"^(.+?),.*"
				}
			},
			"Details": { "Success": "Service running (7036)" }
		},
		{
			"Value": 6,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4674,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.ObjectType": "SERVICE OBJECT",
					"Data.ObjectServer": "SC Manager",
					"Data.PrivilegeList": "*SeTakeOwnershipPrivilege*",
					"range_time": [ -500, 20 ]
				},
				"must_not": {
					"personalized": '{"query_string": {"fields": ["Data.SubjectUserName.keyword"],"query": "*$"} }'
				},
				"should": [
					{ "Data.ObjectName": "Transform.ServiceName" },
					{ "Data.ObjectName": "Data.param1" },
					{ "minimum_should_match": 1 }
				]
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"System.Computer",
				"Data.SubjectLogonId",
				"Data.SubjectDomainName",
				"Data.SubjectUserName",
				"Data.SubjectUserSid"
			],
			"Details": { "Success": "Access (4674)" }
		},
		{
			"Value": 4,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 7045,
					"System.ProviderName": "Service Control Manager",
					"System.Computer": "System.Computer",
					"range_time": [ -10000, 10 ]
				},
				"should": [
					{ "Data.RawData": "*{Transform.ServiceName}*" },
					{ "Data.ServiceName": "Transform.ServiceName" },
					{ "Data.RawData": "*{Data.param1}*" },
					{ "Data.ServiceName": "Data.param1" },
					{ "minimum_should_match": 1 }
				]
			},
			"Details": {
				"Success": "Installed (7045)",
				"Fail": "No installation : The service is already installed ?"
			}
		},
		{
			"Value": 4,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4673,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.PrivilegeList": "*SeTcbPrivilege*",
					"Data.Service": "LsaRegisterLogonProcess()",
					"range_time": [ 0, 1000 ]
				}
			},
			"Details": { "Success": "Lsass service called (4673)" }
		},
		{
			"Value": 6,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 4674,
					"System.ProviderName": "Microsoft-Windows-Security-Auditing",
					"System.Computer": "System.Computer",
					"Data.SubjectLogonId": "Data.SubjectLogonId",
					"Data.PrivilegeList": "*SeTakeOwnershipPrivilege*",
					"Data.SubjectDomainName": "Data.SubjectDomainName",
					"Data.SubjectUserSid": "Data.SubjectUserSid",
					"range_time": [ 0, 3600000 ]
				}
			},
			"Fields": [
				{
					"field": "@timestamp",
					"format": "epoch_millis" 
				},
				"Data.ProcessName"
			],
			"Details": { "Success": "Processes launched (4674)" }
		},
		{
			"Value": 1,
			"Optionnal": True,
			"Conditions": {
				"must": {
					"System.EventID#": 7036,
					"System.ProviderName": "Service Control Manager",
					"System.Computer": "System.Computer",
					"range_time": [ 0, 3600000 ]
				},
				"should": [
					{ "Data.RawData.keyword": "{Transform.ServiceName}.*" },
					{ "Data.RawData.keyword": "{Data.param1}.*" },
					{ "Data.param1.keyword": "{Transform.ServiceName}.*" },
					{ "Data.param1.keyword": "{Data.param1}.*" },
					{ "Data.Binary.keyword": ".*1000000" },
					{ "minimum_should_match": 2 }
				]
			},
			"Details": { "Success": "Stopped (7036)" }
		}
	]



	def _display_detection_list_(self, arg_detection):

		for key in arg_detection:
			if key == "Probability":
				print(" "+chr(0x21B3)+f"  Probability : {int(arg_detection[key]*100)} %")
			elif key == "Commands list":
				if len(arg_detection[key]) > 0:
					print(f"  | {key} :")
					for command in arg_detection[key]:
						print("    "+chr(0x21DB)+f" {self._date_from_timestamp_millis_(command['Time'])} - {command['Process']}")
			elif key == "Details":
				tmp = arg_detection[key].replace('\n', '\n    '+chr(0x21B3)+' ')
				print(f"  | {key} : {self.DETECTION_NAME} detection\n    "+chr(0x21B3)+f" {tmp}")
			elif "Time" in key or "time" in key:
				print(f"  | {key} : {self._date_from_timestamp_millis_(arg_detection[key])}")
			else:
				print(f"  | {key} : {arg_detection[key]}")


	def _test_attack_detection_(self, arg_index, arg_detection_list, PART_NAME="", VERBOSITY=1):

		if VERBOSITY > 0: print(f"[ ] Detection of PSExec usage ({PART_NAME})...")

		if isinstance(arg_index, list):
			result = []
			for index in arg_index:
				result_tmp = self._test_attack_(index, arg_detection_list)
				if not isinstance(result_tmp, int):
					for detection in result_tmp: result.append(detection)
		else: result = self._test_attack_(arg_index, arg_detection_list)
		if isinstance(result, int): result = []

		if isinstance(result, int):
			if result != 0: print(f'[-] Error during the detection of PSExec usage ({PART_NAME}) !')
		elif len(result) == 0:
			if VERBOSITY > 1: print(f"[+] Nothing detected ({PART_NAME}) !")
		elif VERBOSITY > 1:
			print(f"[+] Traces of PSExec usage detected ({PART_NAME}) !")
			if VERBOSITY > 2:
				print('')
				for detection in result:
					self._display_detection_list_(detection)
					print('')

		return result

	def _traitment_datas_(self, arg_result, VERBOSITY=1):

		if VERBOSITY > 0: print("[ ] Traitment of received datas...")
		
		attacks = []
		regex_timestamp_decomp = re.compile("^@timestamp[0-9]*_([0-9]*)$")
		regex_key_number_decomp = re.compile("^(.*)_([0-9]*)$")
		result = sorted(arg_result, key=lambda r: int(r['@timestamp']))	# Sort by timestamp

		for psexec_usage in result:

			attack = {}
			attack['Probability'] = psexec_usage.pop('Probability')
			if '@timestamp0' in psexec_usage: attack['Time'] = int(psexec_usage.pop('@timestamp0'))
			else: attack['Time'] = int(psexec_usage.pop('@timestamp'))
			attack['DetectionName'] = psexec_usage.pop('DetectionName')

			try: attack['Details'] = psexec_usage.pop('Details')
			except: pass

			attack['Target'] = psexec_usage.pop('System.Computer')
			try:
				if 'Transform.ServiceName' in psexec_usage: attack['Service'] = psexec_usage.pop('Transform.ServiceName')
				else: attack['Service'] = psexec_usage.pop('Data.param1')
				if attack['Service'] == "PSEXESVC":
					if attack['Probability'] < 0.9: attack['Probability']=0.9
					attack['Details']+=',\nService named PSEXESVC'
			except: pass

			try: attack['SubjectDomain'] = psexec_usage.pop('Data.SubjectDomainName')
			except: pass
			try: attack['SubjectUser'] = psexec_usage.pop('Data.SubjectUserName')
			except: pass
			try: attack['SubjectUserSid'] = psexec_usage.pop('Data.SubjectUserSid')
			except: pass
			try: attack['SubjectLogonId'] = psexec_usage.pop('Data.SubjectLogonId')
			except: pass

			### Commands list ###

			l_timestamp_num = {}
			for key, value in psexec_usage.items():
				result = regex_timestamp_decomp.search(key)
				if result is not None:
					if result[1] not in l_timestamp_num: l_timestamp_num[result[1]] = {}
					l_timestamp_num[result[1]]['Time'] = int(value)
				else:
					result = regex_key_number_decomp.search(key)
					if result is not None:
						if result[2] not in l_timestamp_num: l_timestamp_num[result[2]] = {}
						l_timestamp_num[result[2]][result[1]] = value

			l_process = []
			for key, value in l_timestamp_num.items():
				if 'Data.ProcessName' in value:
					if VERBOSITY < 2 and 'services.exe' in value['Data.ProcessName']: continue
					tmp = { 'Time': value['Time'] }
					tmp['Process'] = value['Data.ProcessName']
					l_process.append(tmp)
			l_process = sorted(l_process, key=lambda proc: proc['Time'])

			## Double suppression ##

			i_process = 1
			len_process = len(l_process)
			while i_process < len_process:
				if l_process[i_process]['Time'] == l_process[i_process-1]['Time'] and l_process[i_process]['Process'] == l_process[i_process-1]['Process']:
					l_process.pop(i_process)
					len_process -= 1
					continue
				i_process += 1

			### Update of attack information list ###

			for proc in l_process:
				try:
					if proc['Time'] > attack['EndTime']: attack['EndTime'] = proc['Time']
				except:
					if proc['Time'] > attack['Time']: attack['EndTime'] = int(proc['Time'])

			attack['Commands list'] = l_process

			attacks.append(attack)

		return attacks



	def test_psexec_use(self, arg_index, MIN_PERCENT=None, VERBOSITY=1):
		if MIN_PERCENT is None: MIN_PERCENT=0.6
		
		if VERBOSITY > 0: print("[ ] Detection of PSExec usage...")

		result = self._test_attack_detection_(arg_index, self.DETECTION_PSEXEC, PART_NAME="PSEXEC", VERBOSITY=VERBOSITY)
		
		attacks = self._traitment_datas_(result, VERBOSITY=VERBOSITY)

		### Deletion of low probability ###

		i = 0
		i_len = len(attacks)
		while i < i_len:
			attack = attacks[i]
			if attack['Probability'] < MIN_PERCENT:
				attacks.pop(i)
				i_len-=1
				continue
			i+=1

		### Sorting and display ###

		if len(attacks) > 0:
			if VERBOSITY > 0: print("[ ] Results : ")
			attacks = sorted(attacks, key=lambda attack: -1*attack['Probability'])

			i = 0
			i_len = len(attacks)
			while i < i_len:
				self._display_detection_list_(attacks[i])
				print('')
				i+=1
		else:
			if VERBOSITY > 0: print("[ ] No usage of PSExec detected !")

		return attacks