
from datetime import datetime
from elasticsearch import Elasticsearch, helpers
import json
import re
import warnings


class Attacks_Detector:

	REGEX_FIND_VAR = re.compile(r"{([^{}]+?)}")

	def __init__(self, arg_elk_config, arg_detection_name="-"):
		super(Attacks_Detector, self).__init__()

		self.ELK_CONFIG = arg_elk_config
		self.ES = Elasticsearch(self.ELK_CONFIG)
		self.DETECTION_NAME = str(arg_detection_name)
		self.ERROR_LAST_REQUEST = False

		warnings.filterwarnings('ignore', message='Elasticsearch built-in security features are not enabled')	# SUPPRESSION OF A WARNING FROM ELASTICSEARCH


	'''
		Function which convert timestamp millis to readable date
	'''
	def _date_from_timestamp_millis_(self, arg_timestamp_millis):
		return str(datetime.utcfromtimestamp(int(arg_timestamp_millis)/1000))+' UTC'


	##########################
	# ELASTICSEARCH REQUESTS #
	##########################

	BASE_REQUEST = """
	{
		"query": {
			"bool": {
				"must": [REPLACEMENT_MUST],
				"must_not": [REPLACEMENT_NOT_MUST],
				"should": [REPLACEMENT_SHOULD],
				"filter": [REPLACEMENT_FILTER]
				REPLACEMENT_BOOL_PARAMS
			}
		},
		"fields": REPLACEMENT_FIELDS,
		"sort": [
			{ "@timestamp": {"order": "desc" } }
		],
		"_source": false,
		"size": 10000
	}
	""" 	# Base of each request send to elasticsearch database

	'''
		Function which construct an elasticsearch request from a simplify json :

		From something like :
			{
				"Conditions": {
					"must": {
						"System.EventID#": 4776,
						"System.ProviderName": "System.ProviderName" # System.ProviderName value can be pass in arg_elements
					},
					"must_not": {
						"personalized": '{"query_string": {"fields": ["Data.TargetUserName.keyword"],"query": "*$"} }'
					}
				},
				"Fields": [	# Fields return by the request
					{
						"field": "@timestamp",
						"format": "epoch_millis" 
					},
					"System.Computer"
				],
				"Details": { "Success": "Success message" }
			}
	'''
	def _request_construct_(self, arg_json_request, arg_elements=None):

		request = self.BASE_REQUEST
		bool_params = ""

		if "Conditions" in arg_json_request:

			def bool_construct(list_arg, bool_params="", list_elements=None):
				i = 0
				request_part = ""

				if not isinstance(list_arg, list): tab_arg = [list_arg]
				else: tab_arg = list_arg

				for sub_list in tab_arg:
					for key, value in sub_list.items():
						if key == "minimum_should_match":
							bool_params += ',"minimum_should_match":"'+str(value)+'"'
							continue
						if i > 0: request_part += ','

						if ".keyword" in str(key): func_name="regexp"
						else: func_name="match"

						if key == "personalized": request_part += str(value)
						elif key == "range_time" and list_elements is not None: request_part += '{"range":{"@timestamp": {"gte":'+str(int(list_elements["@timestamp"])+list_arg[key][0])+',"lte": '+str(int(list_elements["@timestamp"])+list_arg[key][1])+'}}}'
						else:

							### Format of the value ###

							if list_elements is not None and value in list_elements: value = str(list_elements[value]).replace('\\', '\\\\')
							else:
								value = str(value).replace('\\', '\\\\')
								if list_elements is not None :
									re_results = self.REGEX_FIND_VAR.findall(value)
									for result in re_results:
										if result in list_elements: value = value.replace('{'+result+'}', list_elements[result])

							### Format of the test ###

							if func_name == "match": request_part += '{"match":{"'+str(key)+'":{"query":"'+value+'","operator":"and","zero_terms_query":"all"}}}'
							else: request_part += '{"'+func_name+'":{"'+str(key)+'": "'+value+'"}}'

						i+=1

				return request_part, bool_params

			if "must" in arg_json_request["Conditions"]:
				tmp, bool_params = bool_construct(arg_json_request["Conditions"]["must"], bool_params=bool_params, list_elements=arg_elements)
				if tmp == "": request = request.replace('REPLACEMENT_MUST', '{"match_all":{}}')
				else: request = request.replace('REPLACEMENT_MUST', tmp)
			else: request = request.replace('REPLACEMENT_MUST', '{"match_all":{}}')

			if "must_not" in arg_json_request["Conditions"]: 
				tmp, bool_params = bool_construct(arg_json_request["Conditions"]["must_not"], bool_params=bool_params, list_elements=arg_elements)
				if tmp == "": request = request.replace('REPLACEMENT_NOT_MUST', '{"match_none":{}}')
				else: request = request.replace('REPLACEMENT_NOT_MUST', tmp)
			else: request = request.replace('REPLACEMENT_NOT_MUST', '{"match_none":{}}')

			if "should" in arg_json_request["Conditions"]: 
				tmp, bool_params = bool_construct(arg_json_request["Conditions"]["should"], bool_params=bool_params, list_elements=arg_elements)
				if tmp == "": request = request.replace('REPLACEMENT_SHOULD', '{"match_all":{}}')
				else: request = request.replace('REPLACEMENT_SHOULD', tmp)
			else: request = request.replace('REPLACEMENT_SHOULD', '{"match_all":{}}')

			if "filter" in arg_json_request["Conditions"]:
				tmp, bool_params = bool_construct(arg_json_request["Conditions"]["filter"], bool_params=bool_params, list_elements=arg_elements)
				if tmp == "": request = request.replace('REPLACEMENT_FILTER', '{"match_all":{}}')
				else: request = request.replace('REPLACEMENT_FILTER', tmp)
			else: request = request.replace('REPLACEMENT_FILTER', '{"match_all":{}}')

		else:
			request = request.replace('REPLACEMENT_MUST', '{"match_all":{}}').replace('REPLACEMENT_NOT_MUST', '{"match_none":{}}').replace('REPLACEMENT_FILTER', '{"match_all":{}}')

		request = request.replace('REPLACEMENT_BOOL_PARAMS', bool_params)
		if "Fields" in arg_json_request: request = request.replace('REPLACEMENT_FIELDS', str(arg_json_request["Fields"]).replace("'", '"'))
		else: request = request.replace('REPLACEMENT_FIELDS', '[]')

		return request


	'''
		Function which will send elasticsearch request and will return interesting result part
	'''
	def _send_request_(self, arg_index, arg_query, REPLACEMENT_TAB={}):

		for key in REPLACEMENT_TAB:
			if not isinstance(REPLACEMENT_TAB[key], str):
				REPLACEMENT_TAB[key] = str(REPLACEMENT_TAB[key])
			arg_query = arg_query.replace(key, REPLACEMENT_TAB[key])

		try:
			result = self.ES.search(index=arg_index, body=json.loads(arg_query))
		except:
			print("[-] Exception returned by elasticsearch sender !")
			self.ERROR_LAST_REQUEST = True
			return -1

		if result['_shards']['total'] != result['_shards']['successful']:
			self.ERROR_LAST_REQUEST = True
			print("[-] Errors appears during the request !")
			if result['_shards']['successful'] == 0:
				return -1

		data = [doc for doc in result['hits']['hits']]
		if len(data) == 0 and 'aggregations' in result:
			data = [result['aggregations']]

		self.ERROR_LAST_REQUEST = False

		return data

	'''
		Function which parse fields from the result of an elasticsearch request
	'''
	def _get_fields_from_request_result_(self, arg_request_result):

		datas = {}
		if 'fields' in arg_request_result:
			fields = arg_request_result['fields']
			for field in fields:
				try: datas[field] = fields[field][0]
				except: datas[field] = fields[field]

		return datas


	###############
	# ATTACK PART #
	###############

	'''
		Function which launch a detection by sending a list of request and return the result with datas recovery
	'''
	def _test_attack_(self, arg_index, arg_detection_chain):

		infos = []
		total_value = 0
		for r in arg_detection_chain:
			try: total_value += r["Value"]
			except: pass

		def after_treatment(arg_treatments, arg_values):
			result = {}
			for fieldname, treatment in arg_treatments.items():
				try:
					key = list(treatment.keys())[0]
					regex = treatment[key]
					r = re.search(regex, arg_values[key])
					result[fieldname] = r[1]
				except: pass
			return result


		request = arg_detection_chain[0]
		request_to_send = self._request_construct_(request)

		result = self._send_request_(arg_index, request_to_send)
		if self.ERROR_LAST_REQUEST: return -1

		for connection in result:
			bool_detection = True
			timestamp_num = 0
			connection_infos = { 'Probability' : request["Value"], "DetectionName" : self.DETECTION_NAME }

			connection_fields = self._get_fields_from_request_result_(connection)

			### Recovery of informations ###

			for key, value in connection_fields.items():
				connection_infos[key] = value
			try: connection_infos['@timestamp'] = int(connection_infos['@timestamp'])
			except: pass

			if 'Details' in request and 'Success' in request['Details']:
				if 'Details' in connection_infos: connection_infos['Details'] += ',\n'+request['Details']['Success']
				else: connection_infos['Details'] = request['Details']['Success']

			if 'Transform' in request: connection_infos.update(after_treatment(request['Transform'], connection_infos))

			i_request = 1
			while i_request < len(arg_detection_chain):
				tmp_request = arg_detection_chain[i_request]
				i_request+=1

				tmp_result = self._send_request_(arg_index, self._request_construct_(tmp_request, connection_infos))
				if not self.ERROR_LAST_REQUEST and len(tmp_result) > 0:
					connection_infos['Probability'] += tmp_request['Value']

					if len(tmp_result) == 1:
						tmp_fields = self._get_fields_from_request_result_(tmp_result[0])

						for key, value in tmp_fields.items():
							if key == "@timestamp":
								connection_infos["@timestamp"+str(timestamp_num)] = connection_infos["@timestamp"]
								timestamp_num += 1
							connection_infos[key] = value
					else:
						i = 0
						timestamp_num += 1
						for tmp_res in tmp_result:
							tmp_fields = self._get_fields_from_request_result_(tmp_res)
							for key, value in tmp_fields.items():
								if key == "@timestamp":
									connection_infos["@timestamp"+str(timestamp_num)+"_"+str(i)] = value
								else: connection_infos[key+"_"+str(i)] = value
							i+=1

					if 'Details' in tmp_request and 'Success' in tmp_request['Details']:
						if 'Details' in connection_infos: connection_infos['Details'] += ',\n'+tmp_request['Details']['Success']
						else: connection_infos['Details'] = tmp_request['Details']['Success']

					if 'Transform' in tmp_request: connection_infos.update(after_treatment(tmp_request['Transform'], connection_infos))

				else:
					if not "Optionnal" in tmp_request or not tmp_request["Optionnal"]:
						bool_detection = False
						break
					else:
						try: 
							if 'Details' in connection_infos: connection_infos['Details'] += ',\n'+tmp_request['Details']['Fail']
							else: connection_infos['Details'] = tmp_request['Details']['Fail']
						except: pass

			connection_infos['Probability'] /= total_value
			if bool_detection: infos.append(connection_infos)


		return infos


	#################
	# CSV CONVERTER #
	#################

	CSV_FIELD_LIST = ['Probability', 'Time', 'EndTime', 'DetectionName', 'Details', 'Host', 'Target', 'DC', 'DestinationIp', 'DestinationPort', 'SourceIp', 'SourcePort', 'SubjectLogonId', 'SubjectDomain', 'SubjectUser', 'SubjectUserSid', 'TargetLogonId', 'TargetDomain', 'TargetUser', 'TargetUserSid', 'Workstation']

	def to_csv(self, arg_elements):

		csv = '"'+self.CSV_FIELD_LIST[0]+'"'
		i = 1
		len_field = len(self.CSV_FIELD_LIST)
		while i < len_field:
			csv += ',"'+str(self.CSV_FIELD_LIST[i])+'"'
			i+=1
		csv += ',"Datas"'

		def element_to_csv(element):
			if self.CSV_FIELD_LIST[0] in element: str_csv = '"'+str(element.pop(self.CSV_FIELD_LIST[0])).replace('"', '\\"')+'"'
			else: str_csv = ''

			i_field = 1
			while i_field < len_field:
				str_csv += ','
				if self.CSV_FIELD_LIST[i_field] in element:
					if 'Time' in self.CSV_FIELD_LIST[i_field]: str_csv += '"'+self._date_from_timestamp_millis_(element.pop(self.CSV_FIELD_LIST[i_field]))+'"'
					else: str_csv += '"'+str(element.pop(self.CSV_FIELD_LIST[i_field])).replace('"', '\\"')+'"'
				i_field += 1

			bool_new_line = False
			str_rest = ',"'
			for key, value in element.items():
				if bool_new_line: str_rest += '\n'
				else: bool_new_line = True
				value = str(value).replace('"', '\\"')
				str_rest += f"{str(key)}={value}"
			str_rest += '"'

			return str_csv+str_rest

		if isinstance(arg_elements, list):
			for element in arg_elements:
				csv+='\n'+element_to_csv(element)
		else: csv+='\n'+element_to_csv(arg_elements)

		return csv


